package com.parse.dxf.service.impl;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.parse.dxf.entity.RoomInfo;
import com.parse.dxf.mapper.RoomInfoMapper;
import com.parse.dxf.service.RoomInfoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class RoomInfoServiceImpl implements RoomInfoService
{
    @Autowired
    private RoomInfoMapper roomInfoMapper;

    @Override
    public int insert(RoomInfo data) {
        return roomInfoMapper.insert(data);
    }


    @Override
    public int insert(List<RoomInfo> datas) {
        roomInfoMapper.insert(datas);
        return 1;
    }

    @Override
    public RoomInfo getRoomInfoById(int id) {
        return roomInfoMapper.selectById(id);
    }

    @Override
    public List<RoomInfo> getRoomsInfoDxfId(int dxf_id) {
        QueryWrapper<RoomInfo> queryWrapper = new QueryWrapper<>();
        queryWrapper.lambda().eq(RoomInfo::getDxf_id, dxf_id);
//        queryWrapper.select("dxf_id").like("dxf_id", dxf_id);
        return roomInfoMapper.selectList(queryWrapper);
    }
}
