package com.parse.dxf.service;


import com.parse.dxf.entity.DxfInfo;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public interface DxfInfoService {
    /**
     * 添加dxf数据
     */
    int insert(DxfInfo data);

    /**
     * 上传dxf文件
     */
    DxfInfo uploadDxf(MultipartFile file);

    /**
     * 上传dxf文件
     */
    DxfInfo getDxfInfoById(int id);

    /**
     * 下载dxf文件
     */
    void downloadFile(String fileName, HttpServletResponse response) throws IOException;
}
