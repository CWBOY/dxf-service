package com.parse.dxf.service;


import com.parse.dxf.entity.RoomInfo;

import java.util.List;

public interface RoomInfoService {

    /**
     * 添加房间数据
     */
    int insert(RoomInfo data);

    /**
     * 添加多个房间数据
     */
    int insert(List<RoomInfo> datas);

    /**
     * 通过id获取房间信息
     */
    RoomInfo getRoomInfoById(int id);

    /**
     * 通过dxf_id获取房间信息
     */
    List<RoomInfo> getRoomsInfoDxfId(int dxf_id);
}
