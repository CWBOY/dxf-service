package com.parse.dxf.mapper;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.parse.dxf.entity.RoomInfo;
import org.apache.ibatis.annotations.Mapper;
@Mapper
public interface RoomInfoMapper extends BaseMapper<RoomInfo> {

}
