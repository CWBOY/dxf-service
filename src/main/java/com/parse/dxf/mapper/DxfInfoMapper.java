package com.parse.dxf.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.parse.dxf.entity.DxfInfo;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface DxfInfoMapper  extends BaseMapper<DxfInfo> {

}
