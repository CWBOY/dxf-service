package com.parse.dxf.controller;


import com.parse.dxf.entity.DxfInfo;
import com.parse.dxf.entity.result.ResponseResult;
import com.parse.dxf.service.DxfInfoService;
import com.parse.dxf.utils.DxfParseUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.xml.sax.SAXException;

import javax.servlet.http.HttpServletResponse;
import java.io.FileNotFoundException;
import java.io.IOException;


@RestController
public class DxfInfoController {

    @Autowired
    private DxfInfoService dxfInfoService;

    @PostMapping("/uploadDxf")
    public ResponseResult<DxfInfo> uploadDxf(@RequestParam("file") MultipartFile file) {
        DxfInfo data=  dxfInfoService.uploadDxf(file);
        return data != null ? ResponseResult.success("上传成功",data) : ResponseResult.fail("上传失败");
    }

    @GetMapping("/id/{userId}")
    public ResponseResult<DxfInfo> uploadDxf1(@PathVariable("userId") int id) {
        DxfInfo data=  dxfInfoService.getDxfInfoById(id);
        return data != null ? ResponseResult.success(data) : ResponseResult.fail("id不存在");
    }

    @GetMapping("files/{fileName}")
    public void downloadFile(@PathVariable("fileName") String fileName, HttpServletResponse response) throws IOException, IOException {
        dxfInfoService.downloadFile(fileName,response);
    }

    @GetMapping("test")
    public void test() throws FileNotFoundException, SAXException {
        String dxfPath="G:\\DXF\\dxfFile\\files\\5f947a09-d6dc-4035-819e-6263ebe57ba3_3025-F11.dxf";
        String svgPath="G:\\DXF\\dxfFile\\files\\5f947a09-d6dc-4035-819e-6263ebe57ba3_3025-F11.svg";
        DxfParseUtils.convertDXFToSVG(dxfPath,svgPath);
    }
}