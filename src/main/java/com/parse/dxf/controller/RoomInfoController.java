package com.parse.dxf.controller;


import com.parse.dxf.entity.RoomInfo;
import com.parse.dxf.entity.result.ResponseResult;
import com.parse.dxf.service.RoomInfoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;


@RestController
public class RoomInfoController {

    @Autowired
    private RoomInfoService roomInfoService;


    @GetMapping("/getRoom/{id}")
    public ResponseResult<RoomInfo> getRoom(@PathVariable("id") int id) {
        RoomInfo data=  roomInfoService.getRoomInfoById(id);
        return data != null ? ResponseResult.success(data) : ResponseResult.fail("id不存在");
    }


    @GetMapping("/getRooms/{dxf_id}")
    public ResponseResult<List<RoomInfo>> getRooms(@PathVariable("dxf_id") int dxf_id) {
        List<RoomInfo> data=  roomInfoService.getRoomsInfoDxfId(dxf_id);
        return ResponseResult.success(data);
    }
}