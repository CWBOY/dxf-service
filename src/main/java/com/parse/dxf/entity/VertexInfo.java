package com.parse.dxf.entity;

import lombok.Data;
import lombok.Getter;

@Getter
@Data
public class VertexInfo {
    private float x;
    private float y;
    private float z;
    private float bulge;
}
