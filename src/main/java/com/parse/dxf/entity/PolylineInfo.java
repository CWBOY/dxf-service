package com.parse.dxf.entity;

import lombok.Data;

import java.util.List;

@Data
public class PolylineInfo {

    private List<VertexInfo> polyline;
    private boolean isColosed;
}
