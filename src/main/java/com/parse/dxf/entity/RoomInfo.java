package com.parse.dxf.entity;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import lombok.Getter;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.extension.handlers.JacksonTypeHandler;

import java.util.List;

@Getter
@Data
@TableName(value = "room_info",autoResultMap = true)
public class RoomInfo {
    /**
     * 房间id
     */
    @TableId(type = IdType.AUTO)
    private Integer id;
    /**
     * 房间dxf_id
     */
    private int dxf_id;
    /**
     * 房间rm_id
     */
    @TableField(value = "rm_id")
    private String rm_id;
    /**
     * 房间名称
     */
    private String name;
    /**
     * 房间面积
     */
    private float area;
    /**
     * 轮廓
     */
    @TableField(value = "polyline",typeHandler = JacksonTypeHandler.class)
    private PolylineInfo polyline;

    /**
     * 表示该属性不为数据库表字段,仅用于计算
     */
    @TableField(exist = false)
    private List<VertexInfo> pts;
}
