package com.parse.dxf.entity;
import com.baomidou.mybatisplus.annotation.*;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;
import lombok.Getter;
import org.apache.ibatis.type.LocalDateTypeHandler;

import java.time.LocalDateTime;
import java.util.Date;

@Getter
@Data
@TableName("dxf_info")
public class DxfInfo {
    @TableId(type = IdType.AUTO)
    private Integer id;
    private String name;
    @TableField(value = "filePath")
    private String filePath;


    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @TableField(value = "upload_time",fill = FieldFill.INSERT)
    private LocalDateTime uploadTime;

}
