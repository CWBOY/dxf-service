package com.parse.dxf;


import com.parse.dxf.entity.RoomInfo;
import com.parse.dxf.utils.DxfParseUtils;
import org.kabeja.dxf.DXFDocument;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.mybatis.spring.annotation.MapperScan;
import org.xml.sax.SAXException;

import java.io.FileNotFoundException;
import java.util.List;

@SpringBootApplication
@MapperScan("com.parse.dxf.mapper")
public class DxfApplication {

    public static void main(String[] args) throws FileNotFoundException, SAXException {

//        String dxfPath="D:\\360MoveData\\Users\\Administrator\\Documents\\WeChat Files\\wxid_k6vm7i1g3fg922\\FileStorage\\File\\2024-12\\11014-F02.dxf";
//        DXFDocument doc= DxfParseUtils.parseDxf(dxfPath);
//        List<RoomInfo> rooms = DxfParseUtils.parseDxfRooms(doc, 1);
          SpringApplication.run(DxfApplication.class, args);


    }

}
